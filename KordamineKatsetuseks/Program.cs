﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KordamineKatsetuseks
{
    static class Program
    {
       
        static void Main(string[] args)
        {
            List<Loom> loomaaed = new List<Loom>();

            Loom l1 = new Loom("ämmelgas", 8);


            loomaaed.Add(l1);

            Koer k1 = new Koer("Pontu", "Granzh");

            loomaaed.Add(k1);

           // foreach (var x in loomaaed) Console.WriteLine(x);

           // foreach (var x in loomaaed) x.TeeHäält();

            Torka(k1);
            Torka(4);

            Kass miisu = new Kass("miisu");

            Lõuna(miisu);
            

            Sepik s = new Sepik();
            Lõuna(s);

            List<Sepik> sepikud = new List<Sepik>();
            sepikud.Add(s);
            




        }

        static void Torka(object x)
        {
            //if (x is Loom)
            //((Loom)x).TeeHäält();

            //Loom l;
            //l = x as Loom;
            //l = x is Loom ? (Loom)x : null;

            (x as Loom)?.TeeHäält();
            

        }

        static void Lõuna(ISöödav x)
        {
            x.Süüakse();
        }

               
    }


    interface ISöödav
    {
        void Süüakse();
    }

    class Sepik : ISöödav
    {
        public void Süüakse()
        {
            Console.WriteLine("keegi nosib sepikut"); 
        }
    }

    class Kass : Loom, ISöödav, IComparable
    {
        public string Nimi;
        public Kass (string nimi) : base("kass", 4)
        {
            Nimi = nimi;
        }

        public bool LõikaJalg()
        {
            
            return --JalgadeArv > 2;
        }



        public override string ToString()
        {
            return $"kiisumiisu {Nimi}";
        }

        public override void TeeHäält()
        {
            if (Mõte) Console.WriteLine($"{Nimi} lööb nurru");
            else Console.WriteLine($"kass {Nimi} kräunub koledasti" );
        }

        void ISöödav.Süüakse()
        {
            Console.WriteLine($"kass {Nimi} pistetakse nahka"); 
        }

        public int CompareTo(object obj)
        {
            return Nimi.CompareTo((obj as Kass)?.Nimi);
        }
    }


    class Koer : Loom
    {
        public string Tõug;
        public string Nimi;
        public Koer(string nimi, string tõug) : base("koer", 4)
        {
            Tõug = tõug;
            Nimi = nimi;
        }

        public override string ToString()
        {
            return $"{Tõug} nimega {Nimi} ";
        }

        public override void TeeHäält()
        {
            Console.WriteLine($"Koer {Nimi} haugub nagu {Tõug}"); ;
        }
    }

    class Loom
    {
        internal string Liik;
        private int _JalgadeArv;
        protected bool Mõte; // hea = true, halb = false

  
        public Loom(string liik = "tundmatu", int jalgadeArv = 4)
        {
            Liik = liik;
            this.JalgadeArv = jalgadeArv;
        }

        public int JalgadeArv
        {
            get { return _JalgadeArv; }
            set { _JalgadeArv = Math.Min( value, 100); }
        }

        public virtual void TeeHäält()
        {
            Console.WriteLine($"{JalgadeArv}-jalgne {Liik} teeeb koledat häält" );
        }

        public override string ToString()
        {
            return $"{this.Liik} {this.JalgadeArv} jalaga";
        }
    }
}
